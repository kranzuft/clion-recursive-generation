# README #

### What is this repository for? ###

This repository is for creating recursively generated fractals, that are transformed into data types that are highly efficient, and quick to render. I wasn't planning on make it public, but I believe it is necessary to be more open.

### How do I get started?

If you are on mac, download homebrew, and then install CMAKE, SDL2, SDL2_Image and SDL2_TTF. 
After that your best bet is to install Jetbrain's CLION, or use the terminal to make the project.

### What platforms does it support?

Currently Ubuntu-based distributions and Apple Mac work, however for platforms other than windows, building should always be possible if you are willing to refactor the SDL2 headers, which tend to be platform-specific from my experience. Consult SDL2 forums for more on this.