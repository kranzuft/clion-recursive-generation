/*
 * =====================================================================================
 *
 *       Filename:  globals.cpp
 *        Project:  Breakpoint
 *
 *    Description:  global variables for project
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * =====================================================================================
 */

// Using SDL2
#include <SDL2/SDL.h>

// Accessing global variables
#include "globals.h"

/* Window being rendered to */
SDL_Window* gWindow = NULL;

/* Renderer for gWindow */
SDL_Renderer* gRenderer = NULL;
