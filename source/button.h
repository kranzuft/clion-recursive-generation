#ifndef BUTTON_H
#define BUTTON_H

#include <string>
#include <SDL_ttf.h>

#include "entity.h"
#include "text.h"

/* --- Button ---
 * Super class for button object that links
 * to submenus */
class Button: public Entity
{
public:
    Button();
    void setText(string, TTF_Font*, SDL_Color, double h = 1);   /* Set Text */
    void setAlpha(int);                         /* Set Button Alpha Level */
    void display(bool highlight = false);
    void input();                               /* Control Input To Button */
    void centreText();                          /* Centre Text on Screen */
    void setEndPoint();
    int getEnd();

private:
    Text title, title_h;    /* Title of button and highlighted title of button */
    int end_x, end_y;       /* End coordinates */
};

/* -- Position Buttons On Screen -- */
void posButtons(int total, Attributes b[]);

#endif
