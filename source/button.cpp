/*
 * =====================================================================================
 *
 *       Filename:  globals.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Button class, for user input
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * =====================================================================================
 */

/* Using SDL2 */
#ifdef LINUX
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif /* LINUX */

#include <iostream>
#include "button.h"

/* Accesing globals variables, and screen functions */
#include "globals.h"
#include "screen.h"

Button::Button() {
    ;
}

/* -- Setup Button --
 * Setup the button for display */
void Button::setText(string text, TTF_Font *font, SDL_Color color, double h) {
    SDL_Color h_color = { (color.r * h), (color.g * h), (color.b * h) };

    // Setup normal title
    title.setup(text, color, font);

    // Setup highlighted title
    title_h.setup(text, h_color, font);
    title_h.setDesc("(highlighted button title)");
}

/* -- Set End Point --
 * Sets the end position of the Button */
void Button::setEndPoint() {
    end_x = x;
    end_y = y;
}

int Button::getEnd() {
    return end_y;
}

/* -- Set Alpha --
 * Set the alpha level of the text of the button */
void Button::setAlpha(int alpha) {
    title.setAlpha(alpha);
    title_h.setAlpha(alpha);
}

// FIXME: Shit code. Must be a cleaner way without defecating more bs.
void Button::centreText() {
    // For normal font
    title.setCoords(
        x + ratio_adj(title.wth()),
        y + ratio_adj(title.hgt(), h)
    );

    // For highlighted font
    title_h.setCoords(
        x + ratio_adj(title_h.wth()),
        y + ratio_adj(title_h.hgt(), h)
    );
}

/* -- Display Button --
 * Displays the button text, and a square
 * behind the text. */
void Button::display(bool highlight) {
    int r, g, b;            /* RGB font colours */
    double mod = 0.62;      /* BG color modifier */
    SDL_Rect button_rect;   /* Button rectangle */

    // Set button rectangle coordinates,
    // and size attributed.
    button_rect.x = x, button_rect.y = y;
    button_rect.w = w, button_rect.h = scr_h - y;
    title.getRgb(r, g, b);

    // Darken RGB font color set and display background rectangle.
    SDL_SetRenderDrawColor(gRenderer, (r * mod), (g * mod), (b * mod), 0xFF);
    SDL_RenderFillRect(gRenderer, &button_rect);

    // Render text
    // Use highlighted font if func argument true
    centreText();
    if (highlight) {
        title_h.render();
    } else {
        title.render();
    }
}

/* -- Position Buttons On Screen --
 * Positions a set of buttons consecutively in a vertical
 * set on the screen. */
void posButtons(int total, Attributes b[]) {
    int height = scr_h/total;

    // For each button, distribute evenly on entire screen
    for (int i = 0; i < total; i++) {
        b[i].x = 0, b[i].y = (height * i);  // Set coordinates
        b[i].w = scr_w;                     // Set width

        // Determine height (last button takes up
        // the rest of the space available).
        if (i < (total - 1)) {
            b[i].h = height;
        } else {
            b[i].h = scr_h - (height*i);
        }
    }
}
