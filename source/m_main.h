#ifndef TITLE_H
#define TITLE_H

/* Using text class */
#include "text.h"
#include "button.h"

const double start_time = 0.5;    /* preferred start time in seconds */
const double tconst = 60 * start_time;

/* --- Main Menu --- */
class MainM {
    public:
        MainM();

        /* Manual Edit Functions */
        void setup();
        void startup();

        /* Running Functions */
        void update(int avg_fps = 60);
        void input(SDL_Event e);

        /* Display Functions */
        void display();

    private:
        int status;         /* Status of the menu */
        int t_alpha;        /* Alpha of text */
        int button_total;
        int count;

        Button buttons[3];
};

#endif
