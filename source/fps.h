#ifndef FPS_H
#define FPS_H

#include "text.h"
#include "timer.h"

const int FRAMES_PER_SECOND = 60;

/* -- Frames Per Second --
 * Caps the framerate using SDL_ticks() */
class FPS {
    public:
        FPS();
        ~FPS();

        void setDisplay(bool);
        void displaySetup(bool = false);	/* Setup fps text for display */

        void update();			/* Update fps */
        void render();			/* Display fps on screen */

        void calcFPS();			/* Calculate current fps */
        int getAvg();			/* returns avg fps */

        void startCap();		/* Start framerate cap */
        void capFrames();		/* Caps the framerate of the program */

        void dispVarStates();
    private:
        bool starting;
        int avg_fps;

        bool displaying;

        PTimer FPSTimer;
        PTimer CapTimer;

        Text FPSText;

        int frame_count;
        int prev_count;

        double next_sec;
};

#endif
