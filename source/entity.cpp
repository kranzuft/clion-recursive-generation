/*
 * =====================================================================================
 *
 *       Filename:  entity.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Super class for instances on screen
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * =====================================================================================
 */

#include <SDL2/SDL.h>
#include <iostream>

#include "entity.h"

/* Constructor */
Entity::Entity() {
    // Default width and height to zero
    w = h = 0;
}

/* -- Get Width -- */
int Entity::wth() {
    return w;
}

/* -- Get Height -- */
int Entity::hgt() {
    return h;
}

/* Set Size */
void Entity::setSize(int width, int height) {
    this->w = width;
    this->h = height;
}

/* -- Setup Entity --
 * Set coordinates and size of entity */
void Entity::setup(int x, int y, int width, int height) {
    setCoords(x, y);
    setSize(width, height);
}

/* -- Point Bound Test -- */
bool Entity::ptBndTest(int x, int y) {
    bool breach = false;

    if ((this->x < x && x < (this->x + w)) &&
        (this->y < y && y < (this->y + h))) {
        breach = true;
    }

    return breach;
}

/* -- Load Attributes --
 * Load the attributes structure that holds
 * size and coordinate information of the entity. */
void Entity::loadAttributes(Attributes a) {
    setup(a.x, a.y, a.w, a.h);
}
