/*
 * =====================================================================================
 *
 *       Filename:  timer.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Custom timer program
 *
 *         Author:  Kilgore
 *
 * =====================================================================================
 */

#include <SDL2/SDL.h>

#include "timer.h"
#include "cmath"

PTimer::PTimer() {
	start_ticks = 0;
	paused_ticks = 0;

	paused = false;
	started = false;
}

/* -- Start Timer -- */
void PTimer::start(int offset) {
	started = true;
	paused = false;	// reset pause status

	// Get the current clock time
	start_ticks = SDL_GetTicks() - offset;
	paused_ticks = 0;
}

void PTimer::stop() {
	// Stop the timer
	started = false;
	paused = false;

	// Clear tick variables
	start_ticks = 0;
	paused_ticks = 0;
}

void PTimer::pause() {
	// If timer running and isn't paused
	if (started && !paused) {
		paused = true;

		paused_ticks  = SDL_GetTicks() - start_ticks;
	}
}

void PTimer::resume() {
	// If timer running and paused
	if (started && paused) {
		paused = false;

		start_ticks = SDL_GetTicks() - paused_ticks;

		paused_ticks = 0;
	}
}

Uint32 PTimer::getTicks() {
	// The actual timer time
	Uint32 time = 0;

	if (started) {
		if (paused) {
			time = paused_ticks;
		} else {
			time = SDL_GetTicks() - start_ticks;
		}
	}

	return time;
}

double PTimer::getSecs(int precision) {
	double p;	/* 10^precision */
	int ticks_p;	/* ticks in p millisecond units */

	if (precision > 3 || precision < 0) {
		return -1;
	}

	p = pow(10, precision);
	ticks_p = getTicks() / ((p<3)? (1000 - p) : 1);

	return (ticks_p / p);
}

bool PTimer::isStarted() {
	return started;
}

bool PTimer::isPaused() {
	return (paused && started);
}
