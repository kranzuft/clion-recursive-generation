/*
 * =====================================================================================
 *
 *       Filename:  menu.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Controls movement through menus
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * =====================================================================================
 */

// Using IO input
#include <cstdio>

#include "menusys.h"
#include "m_main.h"

MenuSys::MenuSys() {
    cur_menu = main_m;
    MainMenu.startup();
}

MenuSys::~MenuSys() {
    printf("Deleting Menu System\n");
}

/* -- Updates Menu System -- */
void MenuSys::update(int avg_fps) {
    switch (cur_menu) {
        // Case: Main Menu
        case main_m:
            MainMenu.update(avg_fps);
            break;
    }
}

/* -- Displays Current Menu -- */
void MenuSys::display() {
    switch (cur_menu) {
        // Case: Main Menu
        case main_m:
            MainMenu.display();
            break;
    }
}

/* -- Input System Input Handler -- */
void MenuSys::input(SDL_Event e) {
    MainMenu.input(e);
}

/* -- Menu System Setup -- */
void MenuSys::setup() {
    MainMenu.setup();
}
