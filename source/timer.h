#ifndef TIMER_H
#define TIMER_H

class PTimer {
    public:
        PTimer();

        /* Various timer actions */
        void start(int = 0);
        void stop();
        void pause();
        void resume();

        /* Gets the timer's time */
        Uint32 getTicks();
        double getSecs(int = 0);

        /* Checks the status of the timer */
        bool isStarted();
        bool isPaused();
    private:
        /* The clock time at start */
        Uint32 start_ticks;

        /* Ticks stored while timer paused */
        Uint32 paused_ticks;

        /* Timer statuses */
        bool paused;
        bool started;
};

#endif
