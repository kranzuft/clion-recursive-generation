#ifndef MENU_H
#define MENU_H

#include "m_main.h"

/* -- Menu System -- */
class MenuSys {
    public:
        MenuSys();
        ~MenuSys();

        void setup();
        void update(int avg_fps);
        void input(SDL_Event e);

        void display();

    private:
        /* Current menu being accessed */
        int cur_menu;

        /* Enumeration for menus and sub-menus */
        enum menus {main_m, setup_m, options_m};

        /* Title menu class */
        MainM MainMenu;
};

#endif
