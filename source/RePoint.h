//
// Created by Aden Prior on 22/06/16.
//

#ifndef RECURSIVEANIMATION_H
#define RECURSIVEANIMATION_H

#include <vector>
using namespace std;

class RePoint;
class ReAnimate;

class ReAnimate {
    public:
        ReAnimate(unsigned int);
        ReAnimate(RePoint);
        ~ReAnimate();

    private:
        RePoint * points;
        unsigned int total;
};

class RePoint {
    friend class ReAnimate;

    public:
        RePoint();
        RePoint(unsigned int, unsigned int);
        RePoint(int, int);
        RePoint(unsigned int, int, int, int, int, int);
        ~RePoint();

        const RePoint & operator=(const RePoint &);

        void setEq(int, int, int, int, int);
        void setPar(int, int);
        void setRel(unsigned int, unsigned int);

        void connectDraw(int = -1, int = -1);
        void populate();
        unsigned int getSize();

        void pointify(RePoint *, unsigned int &, unsigned int, unsigned int = 0) const;

    private:
        unsigned int depth, cd; // relativity
        void getSize(unsigned int &cur);
        int m;      // magnitude
        int px, py; // parent coordinates
        int x, y;   // point
        int a1, a2; // component
        void free();

        RePoint * points; // children
};


#endif // RECURSIVEANIMATION_H