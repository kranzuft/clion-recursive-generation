#ifndef GLOBALS_H
#define GLOBALS_H

// Using SDL framework
#include <SDL2/SDL.h>

/* Program window */
extern SDL_Window* gWindow;

/* Program Renderer */
extern SDL_Renderer* gRenderer;

#endif
