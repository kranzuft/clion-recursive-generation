#include <SDL2/SDL.h>
#include "colour.h"

/* Colors
 * https://color.adobe.com/Blue-color-theme-6177531/
 * https://color.adobe.com/theme-1-color-theme-6170784/
 * https://color.adobe.com/Theme-4-color-theme-6179604/
 */

SDL_Color bg_color = {37, 60, 89};
SDL_Color title_color = {228, 224, 218};
SDL_Color body_color = {132, 153, 177};
SDL_Color misc_color = {91, 90, 140};
SDL_Color grey = {29, 29, 29};
SDL_Color black = {0, 0, 0};

/* Colours */
Colour::Colour(unsigned int r, unsigned int g, unsigned int b) {
    this->red = (r > 255)? 255 : r;
    this->green = (g > 255)? 255 : g;
    this->blue = (b > 255)? 255 : b;
}

unsigned int Colour::r() const {
    return this->red;
}

unsigned int Colour::g() const {
    return this->green;
}

unsigned int Colour::b() const {
    return this->blue;
}

Colour Colour::operator=(const Colour &colour) {
    this->red = colour.r();
    this->green = colour.g();
    this->blue = colour.b();

    return *this;
}

Colour Colour::operator=(unsigned int colour[3]) {
    this->red = colour[0];
    this->green = colour[1];
    this->blue = colour[2];

    return *this;
}

Colour Colour::operator + (const Colour &colour) {
    this->red += colour.r();
    this->blue += colour.b();
    this->green += colour.g();

    return *this;
}
