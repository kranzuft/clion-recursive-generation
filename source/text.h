#ifndef TEXT_H
#define TEXT_H

#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include <string>
using namespace std;

#include "entity.h"
#include "colour.h"

/* Fonts */
extern TTF_Font *title_font;           /* Title Font */
extern TTF_Font *text_font;            /* Body text Font */
extern TTF_Font *button_font;          /* Button title Font */

class Text: public Entity {
    public:
        /* Initialises texture and surface */
        Text();

        /* Deallocates memory */
        ~Text();

        /* Creates image from specified string literal */
        bool updateTexture(TTF_Font *font = text_font);

        /* Render Text to screen */
        void render();

        /* Set text message and description of text object */
        void setText(string text, string desc = "");
        void setDesc(string text);

        void setAlpha(int alpha);

        void getRgb(int &r, int &g, int &b);

        void setColor(SDL_Color color);

        void setRgbColor(int r, int g, int b);

        void setup(string text, SDL_Color color = body_color, TTF_Font *font = text_font);

        /* Deallocates texture */
        void free();

    private:
        SDL_Texture* tTexture;  /* Stores text to be displayed as texture */
        string text, desc;
            SDL_Color color;
};

#endif
