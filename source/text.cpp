/*
 * =====================================================================================
 *
 *       Filename:  text.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Holds global text attributes
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * =====================================================================================
 */


#include <SDL.h>
#include <SDL_ttf.h>
#include <string>

#include "globals.h"
#include "text.h"

/* Fonts */
TTF_Font *title_font = NULL;            /* Title font */
TTF_Font *text_font = NULL;             /* Body text font */
TTF_Font *button_font = NULL;           /* Button title Font */

Text::Text() {
    tTexture = NULL;
    text = " ";
    desc = "";
    SDL_Color temp_color = {0, 0, 0};
    color = temp_color;
}

Text::~Text() {
    // Deallocate
    printf("Deleting texture titled %s %s\n", text.c_str(), desc.c_str());
    free();
}

void Text::setText(string text, string desc) {
    this->text = text;
    this->desc = desc;
}

void Text::setDesc(string desc) {
    this->desc = desc;
}

void Text::setAlpha(int alpha) {
    SDL_SetTextureAlphaMod(tTexture, alpha);
}

void Text::getRgb(int &r, int &g, int &b) {
    r = color.r;
    g = color.g;
    b = color.b;
}
void Text::setColor(SDL_Color color) {
    this->color = color;
}

void Text::setRgbColor(int r, int g, int b) {
    SDL_Color temp_color = {r, g, b};
    color = temp_color;
}

void Text::free() {
    // Free texture if it exists
    if (tTexture != NULL) {
        SDL_DestroyTexture(tTexture);
        tTexture = NULL;
        w = h = 0;
    }
}

bool Text::updateTexture(TTF_Font *font) {
    // Deallocate preexisting texture
    free();

    // Render text surface
    SDL_Surface* tSurface = TTF_RenderText_Blended(font, text.c_str(), color);

    if (tSurface == NULL) {
        printf("tSurface failed to be created! SDL_ttf Error: %s\n", TTF_GetError());
    } else {
        // Create text texture from surface pixels
        tTexture = SDL_CreateTextureFromSurface(gRenderer, tSurface);

        if (tTexture == NULL) {
            printf("tTexture failed to be created! SDL_ttf Error: %s\n", TTF_GetError());
        } else {
            // Set size attributes
            SDL_QueryTexture(tTexture, NULL, NULL, &w, &h);
        }

        // Deallocate surface
        SDL_FreeSurface(tSurface);
        tSurface = NULL;
    }

    return tTexture != NULL;
}

void Text::setup(string text, SDL_Color color, TTF_Font *font) {
    setText(text);
    setColor(color);
    updateTexture(font);
}

void Text::render() {
    //Set rendering space
	SDL_Rect renderQuad = {x, y, w, h};

	//Render to screen
	SDL_RenderCopy(gRenderer, tTexture, NULL, &renderQuad);
}
