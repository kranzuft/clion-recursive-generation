//
// Created by Aden Prior on 22/06/16.
//

#include "SDL2/SDL.h"
#include "RePoint.h"
#include "globals.h"

#include <iostream>
using namespace std;

ReAnimate::ReAnimate(unsigned int size) {
    points = new RePoint[size];
    total = size;
}

ReAnimate::ReAnimate(RePoint p) {
    unsigned int cur = 0;

    points = new RePoint[p.getSize()];
    p.pointify(points, cur, p.getSize());
    total = p.getSize();
}

ReAnimate::~ReAnimate() {
    delete[] points;
}

RePoint::RePoint() {
    depth = cd = 0;
    x = y = a1 = a2 = m = 0;
    points = NULL;
}

RePoint::~RePoint() {
    free();
}

const RePoint & RePoint::operator=(const RePoint &rp) {
    free();
    setRel(rp.depth, rp.cd);
    setEq(rp.x, rp.y, rp.a1, rp.a2, rp.m);
    setPar(rp.px, rp.py);

    if (rp.points != NULL) {
        points = new RePoint[2];
        points[0] = rp.points[0];
        points[1] = rp.points[1];
    } else {
        points = NULL;
    }

    return *this;
}


void RePoint::free() {
    if (points != NULL) {
        points[0].free();
        points[1].free();
    }

    delete[] points;
    points = NULL;
}

RePoint::RePoint(unsigned int d, unsigned int current_d) {
    setRel(d, current_d);
    points = NULL;
}

RePoint::RePoint(unsigned int d, int nx, int ny,
                 int na1, int na2, int m) {
    setRel(d, 0);
    setEq(nx, ny, na1, na2, m);
    points = NULL;
}

void RePoint::setEq(int new_x, int new_y, int new_a1, int new_a2, int new_m) {
    a1 = new_a1;
    a2 = new_a2;

    x = new_x;
    y = new_y;

    m = new_m;
/*
    cout << "(x, y) + (a1, a2)m: (" << x << ", " << y << ")"
                          << " + (" << a1 <<", " << a2<< ")"
                                    << m << endl;
*/
}

void RePoint::setPar(int x, int y) {
    this->px = x;
    this->py = y;
}

void RePoint::setRel(unsigned int d, unsigned int current_d) {
    depth = d;
    cd = current_d;
    // cout << "depth: " << d << endl;
    // cout << "crrnt: " << current_d << endl;
}

unsigned int RePoint::getSize() {
    unsigned int cur = 0;

    getSize(cur);

    return cur;
}

void RePoint::getSize(unsigned int &cur) {
    cur++;

    if (points != NULL) {
        points[0].getSize(cur);
        points[1].getSize(cur);
    }
}

void RePoint::connectDraw(int origin_x, int origin_y) {
    if (cd != 0) {
        SDL_SetRenderDrawColor(gRenderer, (cd*20) + 50, 255, (cd*20) + 50, 0xFF);
        SDL_RenderDrawLine(gRenderer, origin_x, origin_y, x, y);
    }

    if (points != NULL) {
        points[0].connectDraw(x, y);
        points[1].connectDraw(x, y);
    }
}

/* Populates a recursive pointer with data TODO: Generic */
void RePoint::populate() {
    if (points == NULL && cd < depth && m > 0) {
        points = new RePoint[2];

        points[0].setRel(depth, cd + 1);
        points[1].setRel(depth, cd + 1);

        int nm = m * 15 / 21;
        points[0].setEq((x + ((!a1) * nm)), (y + ((!a2) * nm)), !a1, !a2, nm);
        points[1].setEq((x + ((!a1) * (-nm))), (y + ((!a2) * (-nm))), !a1, !a2, nm);

        points[0].setPar(x, y);
        points[1].setPar(x, y);

        points[0].populate();
        points[1].populate();
    }
}

/* You can't use recursive point classes in a linear vector animation class */
void RePoint::pointify(RePoint * s_points, unsigned int &cur, unsigned int tot, unsigned int last) const {
    unsigned int last_current = cur;
    cout << last_current << endl;
    if (cur == 0) {
        s_points[cur] = *this;
        cur = 1;
    } else if (cur < tot) {
        for (int sp = last; sp < last_current; sp++) {
            for (int p = 0; p < 2; p++) {
                s_points[cur++] = s_points[sp].points[p];
            }
        }
    }

    if (cur < (tot - 1) || (cd == depth)) {
        pointify(s_points, cur, tot, last_current);
    }
}
