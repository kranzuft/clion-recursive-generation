/*
 * =====================================================================================
 *
 *       Filename:  sdl_functions.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Initiates and terminates SDL for program
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * =====================================================================================
 */

// Using SDL and SDL_ttf framework
#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include <stdio.h>

/* File header */
#include "sdl_functions.h"

// Accessing global variables and screen attributes
#include "text.h"
#include "screen.h"
#include "globals.h"

void check_size() {
    // Adjust screen resolution for high dpi displays
    SDL_GL_GetDrawableSize(gWindow, &scr_w, &scr_h);

    // Ratio new screen dimensions : old screen dimensions
    scr_s = scr_w / scr_s;
}

/* Initiates SDL with linear texture filtering,
 * accelerated graphics, and vsync */
bool init() {
    bool success = true;

    // Initiates SDL, SDL Window and SDL Renderer.
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
        success = false;
    } else {
        // Set texture filtering to linear
        if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
            printf("Warning: Linear texture filtering not enabled!");
        }

        gWindow = SDL_CreateWindow(
            "Recurs", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            scr_w, scr_h, SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI
            | SDL_WINDOW_RESIZABLE
        );

        if (gWindow == NULL) { // gWindow E.C.
            printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
            success = false;
        }
        else {
            // Create renderer
            // SDL_RENDERER_PRESENTVSYNC for vsync
            gRenderer = SDL_CreateRenderer(gWindow,-1,SDL_RENDERER_ACCELERATED);

            if (gRenderer == NULL) { // gRenderer E.C.
                printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
                success = false;
            } else {
                // Initialise renderer color to Catalina Blue
                SDL_SetRenderDrawColor(gRenderer, 37, 60 , 88, 0xFF);

                check_size();

                printf("w: %d, h: %d, scale: %d\n", scr_w, scr_h, scr_s);
            }

            // Initialise SDL_ttf
            if (TTF_Init() < 0) {
                printf("TTF_Init failed! Error: %s\n", TTF_GetError());
            }
        }
    }

    return success;
}

/* -- Load Media --
 * Loads resources from /resources folder, including:
 *      - fonts
 */
bool loadMedia() {
    bool success = true;

    // Open fonts
    text_font = TTF_OpenFont("resources/Inconsolata.otf", 64);
    title_font = TTF_OpenFont("resources/lobster.ttf", scr_w/20);
    button_font = TTF_OpenFont("resources/lobster.ttf", scr_w/20);

    if (title_font == NULL || text_font == NULL || button_font == NULL) {
        printf("Failed to load all fonts! SDL_ttf Error: %s\n", TTF_GetError());
        success = false;
    }

    return success;
}

/* -- End SDL --
 * Frees SDL media, shuts down SDL */
void end() {
    // Close font
    TTF_CloseFont(title_font);
    title_font = NULL;

    TTF_CloseFont(button_font);
    button_font = NULL;

    TTF_CloseFont(text_font);
    text_font = NULL;

    // Destroy renderer
    SDL_DestroyRenderer(gRenderer);
    gRenderer = NULL;

    // Destroy window
    SDL_DestroyWindow(gWindow);
    gWindow = NULL;

    //Quit SDL subsystems
    SDL_Quit();
    TTF_Quit();
}
