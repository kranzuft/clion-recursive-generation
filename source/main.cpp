/*
 * =============================================================================
 *
 *       Filename:  main.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Productivity program, organising work and break time based
 *					on a ratio determined by user input.
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * =============================================================================
 */

#include <SDL2/SDL.h>
#include <iostream>

#include "globals.h"
#include "fps.h"
#include "sdl_functions.h"
#include "RePoint.h"
#include "screen.h"

using namespace std;

int main(int argc, char* args[]) {
    FPS FrameControl;
    // MenuSys MS;

    // Initialise SDL and creates window
    if (!init()) {
        printf("SDL failed to initialise.\n");
    } else if (!loadMedia()) {
        printf("Media could not be loaded.\n");
    } else {
        bool running = true;    /* Flags program exit in main loop */
        SDL_Event e;            /* Event handler */
        FrameControl.displaySetup(true);

        RePoint rpoint(10, scr_w / 2, scr_h / 2, 0, 1, scr_w * 8 / 32 );
        rpoint.populate();

        cout << rpoint.getSize() << endl;

        // Main loop
        while (running) {
            FrameControl.startCap();

            // Event queue
            while (SDL_PollEvent(&e) != 0) {
                // User requests quit
                if (e.type == SDL_QUIT) {
                    running = false;
                }
            }

            // MS.update(FrameControl.getAvg());

            // Update fps maintainer
            FrameControl.update();

            // Clear renderer w/ Black Pearl
            SDL_SetRenderDrawColor(gRenderer, 37, 60, 89, 0xFF);
            SDL_RenderClear(gRenderer);

            FrameControl.render();
            rpoint.connectDraw(0, 0);
            // MS.display();

            // Update screen
            SDL_RenderPresent(gRenderer);

            // Control FPS by delaying execution
            FrameControl.capFrames();
        }
    }

    // Exit Program
    end();

    return 0;
}
