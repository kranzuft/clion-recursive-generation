/* =======================================================
 * Copyright:	Kranzuft 2016. All rights reserved.
 *  Filename: 	BigInteger.h
 *    Author: 	Aden Prior, ap591@uowmail.edu.au
 * ======================================================= */

#ifndef COLOUR_H
#define COLOUR_H


/* Colours of the program */
extern SDL_Color bg_color;
extern SDL_Color title_color;
extern SDL_Color body_color;
extern SDL_Color misc_color;
extern SDL_Color grey;
extern SDL_Color black;

/* - Represent rgb defined color values - */
class Colour {
    public:
        Colour(unsigned int r, unsigned int g, unsigned int b);

        /* Access */
        unsigned int r() const;
        unsigned int g() const;
        unsigned int b() const;

        /* Overloaded Operators */
        Colour operator=(const Colour &);
        Colour operator=(unsigned int [3]);
        Colour operator+(const Colour &);
    private:
        unsigned int red, green, blue;
};

#endif
