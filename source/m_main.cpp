/*
 * =====================================================================================
 *
 *       Filename:  title.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Prompts user to choose two sub-menus: Begin, Analyse,
 *                  Configure.
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * =====================================================================================
 */

// Using SDL2, IO steam, C time
#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include <cstdio>

#include "m_main.h"

// Accessing globals, screen functions and colour variables.
#include "globals.h"
#include "screen.h"
#include "colour.h"

MainM::MainM() {
    status = 0;
    t_alpha = 0;
    button_total = 3;
    count = 0;
}

/* -- Main Menu Updater --
 * > Updates alpha level of buttons */
void MainM::update(int avg_fps) {
    if (status == 1) {
        double tmod = (avg_fps * start_time); /* Keeps start time ~constant */

        // Increase RGB alpha in around start_time seconds
        t_alpha += (avg_fps > 50)? (255/tmod) : (255/tconst);

        // Ends init phase after fully faded in
        if (t_alpha >= 250) {
            t_alpha = 255;
        }

        // Sets alpha of each button and moves button into position
        for (int i = 0; i < 3; i++) {
            double y_dec;
            buttons[i].setAlpha(t_alpha);

            // If b is not at its end point,
            // move it up screen
            if (buttons[i].getEnd() < buttons[i].gtY()) {
                // set y_dec based on button index
                y_dec = (button_total - i) * scr_h * 0.5;
                y_dec /= (avg_fps > 50)? tmod : tconst;

                // Set decremented coordinates
                buttons[i].setCoords(0, buttons[i].gtY()-y_dec);
            } else {
                // Set final coordinates
                buttons[i].setCoords(0, buttons[i].getEnd());
            }
        }

        if (buttons[0].getEnd() == buttons[0].gtY() && t_alpha == 255) {
            status = 2;
        }
    }
}

void MainM::startup() {
    status = 1;
}

/* -- Main Menu Input --
 * > Prompts user with coordinates of mouse on button press */
void MainM::input(SDL_Event e) {
    int x, y;
    SDL_GetMouseState(&x, &y);

    if (e.type == SDL_MOUSEBUTTONDOWN) {
        if (SDL_BUTTON(SDL_BUTTON_LEFT)) {
            printf("Mouse Down: (%d, %d)\n", x, y);
        }
    }
}

/* -- Main Menu Display -- */
void MainM::display() {
    int x, y;

    getMouseCoords(x, y);

    for (int i = 0; i < 3; i++) {
        buttons[i].display(buttons[i].ptBndTest(x, y));
    }
}

/* -- Main Menu Setup -- */
void MainM::setup() { 
    double highlgt = 1.15;	/* - Highlighted Font Modifier - */
    Attributes attribute_set[button_total];

    /* - Button Colors - */
    SDL_Color analyse_color = {215, 80, 90};    // Mahogany     (Red)
    SDL_Color log_color = {215, 200, 190};      // wheat        (White/Yellow)
    SDL_Color config_color = {120, 150, 180};   // Ship Cove    (Blue)

    posButtons(button_total, attribute_set);

    for (int i = 0; i < button_total; i++) {
        buttons[i].loadAttributes(attribute_set[i]);
        buttons[i].setEndPoint();
        buttons[i].setCoords(buttons[i].wth(), scr_h);
    }

    buttons[0].setText("Analyse", button_font, analyse_color, highlgt);
    buttons[1].setText("Log", button_font, log_color, highlgt);
    buttons[2].setText("Configure", button_font, config_color, highlgt);
}
