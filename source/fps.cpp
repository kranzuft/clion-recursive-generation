/*
 * ============================================================================
 *
 *       Filename:  fps.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Controls framerate of project
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * ============================================================================
 */

#include <SDL2/SDL.h>
#include <cmath>
#include <string>
#include <cstdio>

#include "fps.h"
#include "screen.h"
#include "text.h"

FPS::FPS() {
	// Display fps entirely off initially,
	// execute displaySetup(true) to change.
	displaying = false;	
	starting = true;

	frame_count = 1;	// uncertain what to set this to initially
	prev_count = 0;
	next_sec = FPSTimer.getSecs() + 0.5;
	avg_fps = FRAMES_PER_SECOND;
	FPSTimer.start();
}

FPS::~FPS() {
	printf("Deleting fps object\n");
}

void FPS::setDisplay(bool status) {
	displaying = status;
}

void FPS::displaySetup(bool display_now) {	
	FPSText.setup("0", black);
	FPSText.setCoords(0, (scr_h - FPSText.hgt()));

	if (display_now) {
		displaying = true;
	}

	setDisplay(displaying);
}

void FPS::update() {
	frame_count++;
	calcFPS();
}

int FPS::getAvg() {
	return avg_fps;
}

void FPS::render() {
	if (displaying) {
		FPSText.render();
	}
}

void FPS::calcFPS() {
	double tick_secs = FPSTimer.getSecs(3);
	int frame_dif = frame_count - prev_count;

	if (tick_secs > next_sec) {
		next_sec = tick_secs + 1;
		prev_count = frame_count;

		printf("frame this second: %d\n", frame_dif);	
	}

	if (frame_count % 30 == 0) {
		avg_fps = frame_count / tick_secs;

		if (tick_secs >= 5) {
			frame_count = frame_dif;
			prev_count = 0;	
			FPSTimer.start(1000);
			next_sec = FPSTimer.getSecs(3);
		}

		if (displaying) {
			FPSText.setText(to_string(avg_fps));
			FPSText.updateTexture();
		}
	}
}

void FPS::startCap() {
	CapTimer.start();
}

void FPS::dispVarStates() {
	if (frame_count % 10 == 0) {
		printf("-- Current Variable States --\n");
		printf(
			"fps: %4d, frame_count: %4d, ticks: %10f\n",
			avg_fps, frame_count, FPSTimer.getSecs()
		);
	}
}

void FPS::capFrames() {
	int frame_ticks = CapTimer.getTicks();

	if (frame_ticks < SCREEN_TPF) {
		SDL_Delay(SCREEN_TPF - frame_ticks);
	}
}
